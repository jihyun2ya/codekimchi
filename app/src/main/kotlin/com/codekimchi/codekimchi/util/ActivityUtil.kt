package com.codekimchi.codekimchi.util

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.transition.*
import android.view.View

/**
 * Created by thunder on 2017. 2. 23..
 */

fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
    val transaction = this.supportFragmentManager.beginTransaction()
    transaction.replace(frameId, fragment)
    transaction.commit()
}


fun AppCompatActivity.replaceFragmentWithSharedElement(fragment: Fragment, frameId: Int, view: View, name: String) {
    val transitionSet = TransitionSet()
    transitionSet.addTransition(ChangeBounds())
    transitionSet.setDuration(200)

    fragment.sharedElementEnterTransition = transitionSet
//    fragment.enterTransition = Fade()
//    fragment.exitTransition = Fade()
    fragment.sharedElementReturnTransition = transitionSet

    val transaction = this.supportFragmentManager.beginTransaction()
    transaction.addSharedElement(view, name)
    transaction.replace(frameId, fragment)
    transaction.commit()
}

package com.codekimchi.codekimchi.welcome

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.codekimchi.codekimchi.R
import com.codekimchi.codekimchi.util.replaceFragmentWithSharedElement

/**
 * Created by jihyun on 2017. 3. 17..
 */
class SplashFragment : Fragment() {

    private val ivLogo by lazy {
        view?.findViewById(R.id.welcome_iv_logo) as ImageView
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater?.inflate(R.layout.fragment_splash, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler().postDelayed(Runnable {
            var cur = activity as AppCompatActivity
            cur.replaceFragmentWithSharedElement(WelcomeFragment(), R.id.frame_layout, ivLogo, "welcome_iv_logo")
        }, 3000)
    }
}
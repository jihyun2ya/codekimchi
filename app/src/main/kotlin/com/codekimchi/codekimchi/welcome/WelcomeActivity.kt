package com.codekimchi.codekimchi.welcome

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.codekimchi.codekimchi.R
import com.codekimchi.codekimchi.util.replaceFragment
import com.codekimchi.codekimchi.util.replaceFragmentWithSharedElement

/**
 * Created by thunder on 2017. 3. 15..
 */

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        replaceFragment(SplashFragment(), R.id.frame_layout)
    }
}
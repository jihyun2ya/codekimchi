package com.codekimchi.codekimchi.inbox

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.codekimchi.codekimchi.data.ContentData
import com.codekimchi.codekimchi.explore.ExploreItemHolder
import java.util.*

/**
 * Created by thunder on 2017. 3. 6..
 */

class InboxItemAdapter (private val context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>(), InboxItemModel {

    val itemList: MutableList<InboxItem> = ArrayList()

    override fun addItem(item: InboxItem) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getItem(position: Int): InboxItem? {
        return itemList[position]
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeItem(item: InboxItem) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getItemCount(): Int {
        return 3
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return InboxItemHolder(context, parent)
//
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
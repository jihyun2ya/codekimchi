package com.codekimchi.codekimchi.inbox

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.codekimchi.codekimchi.R

/**
 * Created by thunder on 2017. 2. 23..
 */

class InboxFragment: Fragment(), InboxItemContract.View {

    private var inboxItemPresenter: InboxItemPresenter? = null
    private var inboxItemAdapter: InboxItemAdapter? = null

    private val recyclerView by lazy {
        view?.findViewById(R.id.inbox_recycler_view) as RecyclerView
    }

    companion object {
        fun getInstance() = InboxFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        print("InboxFragment Created")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater?.inflate(R.layout.fragment_inbox, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        inboxItemAdapter = InboxItemAdapter(context)
        inboxItemPresenter = InboxItemPresenter()
        inboxItemPresenter?.view = this

        inboxItemPresenter?.itemModel = inboxItemAdapter

        inboxItemPresenter?.loadDefaultItems()

    }
    override fun onSuccessAddItem(position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccessRemoveItem(position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun adapterOneNotify() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
package com.codekimchi.codekimchi.inbox

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.TextView
import com.codekimchi.codekimchi.R
import com.codekimchi.codekimchi.explore.ExploreItem

/**
 * Created by thunder on 2017. 3. 6..
 */

class InboxItemHolder(val context: Context, parent: ViewGroup?) :
        RecyclerView.ViewHolder(LayoutInflater.from(context).inflate(R.layout.explore_item_view, parent, false)) {


    private val titleTextView by lazy {
        itemView?.findViewById(R.id.inbox_item_title) as TextView
    }

    private val descriptionTextView by lazy {
        itemView?.findViewById(R.id.inbox_item_description) as TextView
    }

    fun bindView(item: InboxItem?, position: Int) {
        itemView?.setOnClickListener {

        }
        titleTextView.text = item?.title
        descriptionTextView.text = item?.description
    }
}
package com.codekimchi.codekimchi.inbox

/**
 * Created by thunder on 2017. 3. 6..
 */

interface InboxItemModel {
    fun addItem(item: InboxItem)
    fun getItem(position: Int): InboxItem?
    fun removeItem(item: InboxItem)
    fun getItemCount(): Int
}
package com.codekimchi.codekimchi.inbox

import com.codekimchi.codekimchi.explore.ExploreItem
import com.codekimchi.codekimchi.inbox.InboxItemContract.Presenter

/**
 * Created by thunder on 2017. 3. 6..
 */

class InboxItemPresenter: Presenter {
    override var view: InboxItemContract.View? = null
    override var itemModel: InboxItemModel? = null

    override fun loadDefaultItems() {
        for (position in 0..5) {
            itemModel?.addItem(InboxItem("name" + position, "ca"))
        }
        view?.adapterOneNotify()
    }
}
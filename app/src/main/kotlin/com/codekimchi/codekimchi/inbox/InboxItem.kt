package com.codekimchi.codekimchi.inbox

/**
 * Created by thunder on 2017. 3. 6..
 */

data class InboxItem(var title: String,
                     var description: String)
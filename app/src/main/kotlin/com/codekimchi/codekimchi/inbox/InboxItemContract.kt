package com.codekimchi.codekimchi.inbox

/**
 * Created by thunder on 2017. 3. 6..
 */

interface InboxItemContract {

    interface View {
        fun onSuccessAddItem(position: Int)
        fun onSuccessRemoveItem(position: Int)
        fun adapterOneNotify()
    }

    interface Presenter {
        var view: View?
        var itemModel: InboxItemModel?
        fun loadDefaultItems()
    }
}
package com.codekimchi.codekimchi.data

/**
 * Created by thunder on 2017. 3. 13..
 */

data class Author (
        val name: String,
        val id: Int
)
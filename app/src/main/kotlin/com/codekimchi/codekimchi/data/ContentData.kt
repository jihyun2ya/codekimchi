package com.codekimchi.codekimchi.data

/**
 * Created by thunder on 2017. 3. 13..
 */

data class ContentData (
        val content: String,
        val title: String,
        val author: Author,
        val id: Int
)

package com.codekimchi.codekimchi.data

import com.codekimchi.codekimchi.network.CodeKimchiServiceInterface
import com.codekimchi.codekimchi.network.createRetrofit
/**
 * Created by thunder on 2017. 3. 8..
 */

object ContentDataSource {
    val KIMCHI_URL = "http://codekimchi.com/"

    private val serviceInterface: CodeKimchiServiceInterface

    init {
        serviceInterface = createRetrofit(CodeKimchiServiceInterface::class.java, KIMCHI_URL)
    }

    fun getContent(id: Int) = serviceInterface.getContent(id)

    fun getContents() = serviceInterface.getContents()
}

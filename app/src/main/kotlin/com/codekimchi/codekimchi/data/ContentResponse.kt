package com.codekimchi.codekimchi.data
import com.codekimchi.codekimchi.data.ContentData
/**
 * Created by thunder on 2017. 3. 8..
 */

data class ContentResponse(
        val data: ContentData
)

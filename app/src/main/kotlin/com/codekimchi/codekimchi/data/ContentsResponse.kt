package com.codekimchi.codekimchi.data

/**
 * Created by thunder on 2017. 3. 8..
 */

data class ContentsResponse(
        val data: Array<ContentData>
)
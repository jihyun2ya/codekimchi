package com.codekimchi.codekimchi.main;

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity

import com.codekimchi.codekimchi.R
import com.codekimchi.codekimchi.explore.ExploreFragment
import com.codekimchi.codekimchi.inbox.InboxFragment
import com.codekimchi.codekimchi.setting.SettingFragment
import android.support.v7.widget.Toolbar

/**
 * Created by thunder on 2017. 2. 23..
 */

class MainActivity : AppCompatActivity() {

    var pagerAdapter: MainPagerAdapter? = null

    private val toolbar: Toolbar by lazy {
        findViewById(R.id.toolbar_main) as Toolbar
    }

    private val tabLayout: TabLayout by lazy {
        findViewById(R.id.tab_main) as TabLayout
    }

    private val pager: ViewPager by lazy {
        findViewById(R.id.pager_main) as ViewPager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        pagerAdapter = MainPagerAdapter(supportFragmentManager)
        pager?.adapter = pagerAdapter
        pager?.offscreenPageLimit = 3
        tabLayout?.setupWithViewPager(pager)

        tabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
                pager.setCurrentItem(tab?.getPosition()!!);
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                pager.setCurrentItem(tab?.getPosition()!!);
            }

        })
        pager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                //throw UnsupportedOperationException()
                // your code
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                //throw UnsupportedOperationException()
                // your code
            }
            // 6. when some views conflict with swipe back , you should do these, for example:
            override fun onPageSelected(position: Int) {
                if (position != 0) {
                    // if the current view page is not the first, make 'vp' receive touch event.

                } else {
                    // the current page return to the first one, make 'swipe back' receive touch event.

                }
                // your code
            }
        })
    }


    inner class MainPagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {

        override fun getPageTitle(position: Int): CharSequence {
            when (position) {
                0 -> return "EXPLORE"
                1 -> return "INBOX"
                2 -> return "SETTING"
                else -> return "UNKNOWN"
            }
        }

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> {
                    return ExploreFragment.getInstance()
                }
                1 -> {
                    return InboxFragment.getInstance()
                }
                2 -> {
                    return SettingFragment.getInstance()
                }
            }
            return null
        }

        override fun getCount(): Int = 3
    }
}

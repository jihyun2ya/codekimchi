package com.codekimchi.codekimchi.setting

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.codekimchi.codekimchi.R
import com.codekimchi.codekimchi.base.BaseFragment
import com.codekimchi.codekimchi.inbox.InboxFragment

/**
 * Created by thunder on 2017. 2. 24..
 */
class SettingFragment: Fragment() {
    companion object {
        fun getInstance() = SettingFragment()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        print("SettingFragment Created")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater?.inflate(R.layout.fragment_setting, container, false)
}


package com.codekimchi.codekimchi.network

import com.codekimchi.codekimchi.data.ContentResponse
import com.codekimchi.codekimchi.data.ContentsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by jihyun on 2017. 3. 5..
 */

interface CodeKimchiServiceInterface {
    @GET("/contents/{contentId}")
    fun getContent(@Path("contentId") contentId: Int): Call<ContentResponse>

    @GET("/contents")
    fun getContents(): Call<ContentsResponse>
}
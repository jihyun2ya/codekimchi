package com.codekimchi.codekimchi.explore

import com.codekimchi.codekimchi.data.ContentDataSource

/**
 * Created by jihyun on 2017. 3. 5..
 */

interface ExploreItemContract {

    interface View {
        fun refresh()
        fun onSuccessAddItem(position: Int)
        fun onSuccessRemoveItem(position: Int)
    }


    interface Presenter {
        var view: View?
        var itemModel: ExploreItemModel?
        var contentData: ContentDataSource?
//        fun loadDefaultItems()
        fun getContents()
//        fun adapterOneAddItem()
    }
}
package com.codekimchi.codekimchi.explore

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.codekimchi.codekimchi.R
import com.codekimchi.codekimchi.data.ContentData

/**
 * Created by jihyun on 2017. 3. 5..
 */

class ExploreItemHolder(val context: Context, parent: ViewGroup?) :
        RecyclerView.ViewHolder(LayoutInflater.from(context).inflate(R.layout.explore_item_view, parent, false)) {

    private val titleTextView by lazy {
        itemView?.findViewById(R.id.explore_item_title) as TextView
    }

    private val descriptionTextView by lazy {
        itemView?.findViewById(R.id.explore_item_description) as TextView
    }

    fun bindView(item: ContentData?, position: Int) {
        itemView?.setOnClickListener {

        }
        titleTextView.text = item?.title
        descriptionTextView.text = item?.title
    }
}
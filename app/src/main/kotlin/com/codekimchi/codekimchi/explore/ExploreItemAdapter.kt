package com.codekimchi.codekimchi.explore

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.codekimchi.codekimchi.data.ContentData
import java.util.*

/**
 * Created by jihyun on 2017. 3. 5..
 */

class ExploreItemAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ExploreItemModel {

    val itemList: MutableList<ContentData> = ArrayList()
    // 뷰 생성 시 최초 한 번
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return ExploreItemHolder(context, parent)
    }

    // 뷰 갱신 시
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
//        when (getItem(position)){
        (holder as ExploreItemHolder)?.bindView(getItem(position), position)
//        }
    }

    override fun getItemCount() = itemList.size

    override fun addItem(item: ContentData) {
        itemList.add(item)
//        Toast.makeText(context, item.name, Toast.LENGTH_SHORT).show()
    }

    override fun getItem(position: Int) = itemList[position]

    override fun removeItem(item: ContentData) {
        itemList.remove(item)
    }
}
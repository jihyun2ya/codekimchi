package com.codekimchi.codekimchi.explore

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.codekimchi.codekimchi.R
import com.codekimchi.codekimchi.base.BaseFragment
import android.widget.Toast
import com.codekimchi.codekimchi.data.ContentDataSource


/**
 * Created by thunder on 2017. 2. 24..
 */

class ExploreFragment : Fragment(), ExploreItemContract.View {

    companion object {
        fun getInstance() = ExploreFragment()
    }

    private var exploreItemPresenter: ExploreItemPresenter? = null
    private var exploreItemAdapter: ExploreItemAdapter? = null

    private val recyclerView by lazy {
        view?.findViewById(R.id.explore_recycler_view) as RecyclerView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        print("ExploreFragment Created")
        Log.d("TAG", "created")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View?
    = inflater?.inflate(R.layout.fragment_explore, container, false)


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("TAG", "onViewCreated")

        exploreItemAdapter = ExploreItemAdapter(context)
        exploreItemPresenter = ExploreItemPresenter()
        exploreItemPresenter?.view = this

        exploreItemPresenter?.itemModel = exploreItemAdapter
        recyclerView?.adapter = exploreItemAdapter

        exploreItemPresenter?.contentData = ContentDataSource


        exploreItemPresenter?.getContents()
//        exploreItemPresenter?.loadDefaultItems()

//        btnAdd.setOnClickListener {
//            itemPresenter?.adapterOneAddItem()
//        }
    }


    override fun refresh() {
        exploreItemAdapter?.notifyDataSetChanged()
    }

    override fun onSuccessAddItem(position: Int) {

    }

    override fun onSuccessRemoveItem(position: Int) {
    }

}
package com.codekimchi.codekimchi.explore

/**
 * Created by jihyun on 2017. 3. 5..
 */

data class ExploreItem (var title: String,
                        var description: String,
                        var subscribed: Boolean)
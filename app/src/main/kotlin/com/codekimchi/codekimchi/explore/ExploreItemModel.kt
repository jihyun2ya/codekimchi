package com.codekimchi.codekimchi.explore

import com.codekimchi.codekimchi.data.ContentData

/**
 * Created by jihyun on 2017. 3. 5..
 */

interface ExploreItemModel {
    fun addItem(item: ContentData)
    fun getItem(position: Int): ContentData?
    fun removeItem(item: ContentData)
    fun getItemCount(): Int
}
package com.codekimchi.codekimchi.explore

import com.codekimchi.codekimchi.data.ContentData
import com.codekimchi.codekimchi.data.ContentDataSource
import com.codekimchi.codekimchi.data.ContentsResponse
import com.codekimchi.codekimchi.explore.ExploreItemContract.Presenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jihyun on 2017. 3. 5..
 */

class ExploreItemPresenter: Presenter {

    override var view: ExploreItemContract.View? = null
    override var itemModel: ExploreItemModel? = null
    override var contentData: ContentDataSource? = null

    override fun getContents() {
        contentData?.getContents()?.enqueue(object : Callback<ContentsResponse> {
            override fun onResponse(call: Call<ContentsResponse>?, response: Response<ContentsResponse>?) {
                val contentResponse = response?.body()
                contentResponse?.data?.forEach {

                    itemModel?.addItem(it)
                }
                view?.refresh()
            }

            override fun onFailure(call: Call<ContentsResponse>?, t: Throwable?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })
    }

//    override fun adapterOneAddItem() {
//        itemModel?.addItem(ContentData("name2222", "ca", false))
//        itemModel?.addItem(ExploreItem("name3333", "ca", false))
//        view?.adapterOneNotify()
//        view?.onSuccessAddItem(itemModel!!.getItemCount())
//    }getItemCount

}

